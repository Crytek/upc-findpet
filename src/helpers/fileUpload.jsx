const fileUpload = async (file) => {
  const cloudUrl = import.meta.env.VITE_API_UPLOAD_IMAGE;

  const formData = new FormData();
  formData.append('api_key', '626646982626162');
  formData.append('upload_preset', 'find-pet');
  formData.append('file', file);

  try {
    const resp = await fetch(cloudUrl, {
      method: 'POST',
      body: formData,
    });

    if (resp.ok) {
      const cloudResp = await resp.json();
      return cloudResp.secure_url;
    } else {
      return null;
      // throw await resp.json();
    }
  } catch (err) {
    throw err;
  }
};

export default fileUpload;
