import React, { useState } from 'react';
import { Navigate, useNavigate } from 'react-router-dom';

import Swal from 'sweetalert2';
import withReactContent from 'sweetalert2-react-content';
import 'sweetalert2/src/sweetalert2.scss';

import useFetchProducts from '../hook/useGetFetchData';
// import { fileUpload } from '../helpers/FileUpload';
import fileUpload from '../helpers/fileUpload';

import Banner from '../components/Banner';
import Header from '../components/Header';

const initialForm = {
  categoria_Id: null,
  titulo: '',
  descripcion: '',
  tipoReporte_Id: null,
  clasificacion_Id: null,
  raza_Id: null,
  lugar: '',
  url_Imagen: '',
  usuario_Id: 1,
  CreateUser: new Date(),
  UpdateUser: new Date(),
};

const PublicationCreate = () => {
  const MySwal = withReactContent(Swal);
  const [form, setForm] = useState(initialForm);
  const [imageFile, setImageFile] = useState(null);
  const [previewImage, setPreviewImage] = useState(null);
  const [disabled, isDisabled] = useState(false);

  const navigate = useNavigate();

  const dataCategories = useFetchProducts('/api/Categoria');
  const dataRaza = useFetchProducts('/api/Raza');
  const dataClasificacion = useFetchProducts('/api/Clasificacion');
  const dataTipoReporte = useFetchProducts('/api/TipoReporte');

  const handleChange = (e) => {
    const { name, value } = e.target;

    setForm({
      ...form,
      [name]: value,
    });
  };

  const handleChangeNumber = (e) => {
    const { name, value } = e.target;

    setForm({
      ...form,
      [name]: Number(value),
    });
  };

  const handleChangeInputFile = (e) => {
    const file = e.target.files[0];
    const reader = new FileReader();
    const url = reader.readAsDataURL(file);
    const image = URL.createObjectURL(file);

    const fileSize = file.size;
    const sizekiloByte = parseInt(fileSize / 1024);
    const sizeFileInput = document
      .querySelector('#inputFile')
      .getAttribute('size');

    if (sizekiloByte > sizeFileInput) {
      MySwal.fire({
        icon: 'info',
        title: <span>La imagen seleccionada supera los 2 megabyte</span>,
        footer: '2MB(megabyte) = 2048Kb',
      });

      setImageFile(null);
      return false;
    }

    setImageFile(file);
    setPreviewImage(image);
  };

  const handleSubmit = async (e) => {
    e.preventDefault();

    isDisabled(true);

    if (imageFile == null) {
      MySwal.fire({
        icon: 'info',
        title: <span>Seleccione una imagen menor a 2 megabyte</span>,
        footer: '2MB(megabyte) = 2048Kb',
      });

      isDisabled(false);
      return;
    }

    const urlImage = await fileUpload(imageFile);

    setForm({
      ...form,
      url_Imagen: urlImage,
    });

    const resp = await fetch(import.meta.env.VITE_API_URL + '/api/Public', {
      method: 'POST',
      headers: { 'Content-Type': 'application/json' },
      body: JSON.stringify({ ...form, url_Imagen: urlImage }),
    });

    if (resp.ok) {
      const data = await resp.json();
      navigate('/');
    } else {
      isDisabled(false);
    }
  };

  return (
    <main id="publication-create">
      <Header />
      <Banner />

      <section id="publication-create-content" className="container">
        <h1>NUEVA PUBLICACIÓN</h1>

        {/* <pre>{JSON.stringify(form, null, 2)}</pre> */}

        <form onSubmit={handleSubmit}>
          <div className="form-group">
            <select
              id=""
              className="form-control"
              name="categoria_Id"
              defaultValue=""
              onChange={handleChangeNumber}
              required
            >
              <option value="" disabled>
                Selecciona una categoría
              </option>
              {dataCategories.data.map(({ descripcion, id }) => (
                <option value={id} key={id}>
                  {descripcion}
                </option>
              ))}
            </select>
          </div>
          <div className="form-group">
            <input
              className="form-control"
              type="text"
              name="titulo"
              placeholder="Ingresa un Título"
              maxLength={30}
              onChange={handleChange}
              required
            />
          </div>
          <div className="form-group">
            <textarea
              className="form-control"
              id=""
              name="descripcion"
              cols="30"
              rows="5"
              placeholder="Ingresa una descripción"
              maxLength={1000}
              onChange={handleChange}
              required
            ></textarea>
          </div>

          <p>Tipo de reporte:</p>
          <div className="form-group-row">
            {dataTipoReporte.data.map(({ descripcion, id }) => (
              <div className="form-group group-radio" key={id}>
                <input
                  id={`reporte_${id}`}
                  type="radio"
                  name="tipoReporte_Id"
                  value={id}
                  onChange={handleChangeNumber}
                  required
                />
                <label htmlFor={`reporte_${id}`}>{descripcion}</label>
              </div>
            ))}
          </div>
          <div className="form-group">
            <select
              id=""
              className="form-control"
              name="clasificacion_Id"
              defaultValue=""
              onChange={handleChangeNumber}
              required
            >
              <option value="" disabled>
                Selecciona una clasificación
              </option>
              {dataClasificacion.data.map(({ descripcion, id }) => (
                <option value={id} key={id}>
                  {descripcion}
                </option>
              ))}
            </select>
          </div>
          <div className="form-group">
            <select
              id=""
              className="form-control"
              name="raza_Id"
              defaultValue=""
              onChange={handleChangeNumber}
              required
            >
              <option value="" disabled>
                Selecciona una raza
              </option>
              {dataRaza.data.map(({ descripcion, id }) => (
                <option value={id} key={id}>
                  {descripcion}
                </option>
              ))}
            </select>
          </div>
          <div className="form-group">
            <input
              className="form-control"
              type="text"
              name="lugar"
              placeholder="Ingresa una dirección"
              onChange={handleChange}
              required
            />
          </div>
          <div className="form-group">
            <input
              id="inputFile"
              className="form-control"
              type="file"
              name=""
              onChange={handleChangeInputFile}
              accept="image/*"
              size="2048"
              required
            />
          </div>
          {previewImage && (
            <picture className="preview-image">
              <img src={previewImage} alt="" />
            </picture>
          )}
          <div className="button-center">
            <button className="btn" type="submit" disabled={disabled}>
              Publicar
            </button>
          </div>
        </form>
      </section>
    </main>
  );
};

export default PublicationCreate;
