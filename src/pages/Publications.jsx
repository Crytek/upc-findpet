import React, { useEffect, useState } from 'react';
import { Link } from 'react-router-dom';

import getFetchData from '../services/getFetchData';

import Header from '../components/Header';
import Banner from '../components/Banner';
import Loader from '../components/Loader/Loader';
import ModalFilter from '../components/ModalFilter';
import ModalBannerAds from '../components/ModalBannerAds';

import imgCategory1 from '../images/category-1.png';
import imgCategory2 from '../images/category-2.png';
import imgCategory3 from '../images/category-3.png';
import imgCategory4 from '../images/category-4.png';

import imgSearch from '../images/search.png';
import imgAdd from '../images/add.png';
import icoFilter from '../images/filter.svg';

import imgDefault from '../images/image-placeholder.png';

// ALL, letra => BUSCAR; FILTRO

const initial = {
  data: [],
  loader: true,
};

const Publications = () => {
  const [tipoFiltro, setTipoFiltro] = useState('ALL');
  const [publications, setPublications] = useState(initial);
  const [searchField, setSearchField] = useState('');

  const [modalFilter, setModalFilter] = useState(false);
  const [modalBannerAds, setModalBannerAds] = useState(true);

  useEffect(() => {
    setPublications(initial);

    getFetchData(`/api/Public?tipoFiltro=${tipoFiltro}`)
      .then((response) => {
        console.log('response:', response);

        setPublications({
          data: response,
          loader: false,
        });
      })
      .catch((error) => console.error(error));
  }, [tipoFiltro]);

  const handleChange = (e) => {
    setSearchField(e.target.value);
  };

  const handleSubmit = (e) => {
    e.preventDefault();
    const filterBuscar = `BUSCAR&descripcion_filtro=${searchField}`;
    const filter = searchField ? filterBuscar : 'ALL';

    setTipoFiltro(filter);
  };

  const handleSubmitFilter = (form) => {
    console.log('formFilter:', form);

    const filterFiltro = `FILTRO&descripcion_filtro=${searchField}&categoria_id=${form.categoria_id}&tipAnimal_id=${form.tipAnimal_id}&raza_id=${form.raza_id}&fec_inicio=${form.fec_inicio}&fec_final=${form.fec_final}`;

    console.log('filterFiltro:', filterFiltro);
    setModalFilter(false);
    setTipoFiltro(filterFiltro);
  };

  return (
    <main id="page-publications">
      <Header />
      <Banner />
      <section id="publications-content" className="container">
        <section id="categories">
          <a href="#!" className="category item-disabled">
            <img src={imgCategory1} alt="Encontrar mascota" />
            <p>Perdidas</p>
          </a>

          <a href="#!" className="category item-disabled">
            <img src={imgCategory2} alt="Buscar mascota" />
            <p>Adopciones</p>
          </a>

          <a href="#!" className="category item-disabled">
            <img src={imgCategory3} alt="Notas sobre mascota" />
            <p>Comunidad</p>
          </a>

          <a href="#!" className="category item-disabled">
            <img src={imgCategory4} alt="Servicios para mascota" />
            <p>Servicios</p>
          </a>
        </section>

        <section id="search">
          <form onSubmit={handleSubmit}>
            <input
              className="form-control"
              type="search"
              placeholder="Buscar..."
              onChange={handleChange}
            />
            <button>
              <img src={imgSearch} alt="Buscar ..." />
            </button>

            <button type="button" onClick={() => setModalFilter(true)}>
              <img src={icoFilter} alt="ico filter" />
            </button>
          </form>
        </section>

        <section id="publications-list">
          <div className="top">
            <h2>Publicaciones</h2>

            <Link to="/publicacion/crear">
              <img src={imgAdd} alt="Agregar" width={30} />
            </Link>
          </div>

          <div className="items">
            {publications.loader && <Loader />}
            {publications.data.map((publi) => (
              <div className="item" key={publi.idPublicacion}>
                <div className="picture">
                  <img src={publi.imagen ?? imgDefault} alt="Image" />
                </div>

                <div className="information">
                  <h3 className="title">{publi.titulo}</h3>
                  <p className="description">{publi.descripcion}</p>
                  {/* <p className="date">31/01/2022</p> */}
                </div>
              </div>
            ))}

            {publications.data.length == 0 && !publications.loader && (
              <p>No se encontraron resultados.</p>
            )}
          </div>
        </section>
      </section>

      {modalFilter && (
        <ModalFilter
          setModalFilter={setModalFilter}
          handleSubmitFilter={handleSubmitFilter}
        />
      )}

      {modalBannerAds && (
        <ModalBannerAds setModalBannerAds={setModalBannerAds} />
      )}
    </main>
  );
};

export default Publications;
