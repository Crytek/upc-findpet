import React, { useState } from 'react';
import { Link, useNavigate } from 'react-router-dom';

import Swal from 'sweetalert2';
import withReactContent from 'sweetalert2-react-content';
import 'sweetalert2/src/sweetalert2.scss';

import imgLogo from '../../images/logo.png';
import icoEye from '../../images/eye.svg';

const initialForm = {
  nombreCompleto: '',
  apellidos: '',
  correo: '',
  celular: null,
  clave: '',
  credenciales: '',
  CreateUser: new Date(),
  UpdateUser: new Date(),
};

const Register = () => {
  const MySwal = withReactContent(Swal);
  const navigate = useNavigate();

  const [form, setForm] = useState(initialForm);
  const [disabled, isDisabled] = useState(false);
  const [showPass, setShowPass] = useState(false);

  const handleChange = (e) => {
    const { name, value } = e.target;

    setForm({
      ...form,
      [name]: value,
    });
  };

  const handleSubmit = async (e) => {
    e.preventDefault();
    isDisabled(true);

    const resp = await fetch(import.meta.env.VITE_API_URL + '/api/User', {
      method: 'POST',
      headers: { 'Content-Type': 'application/json' },
      body: JSON.stringify(form),
    });

    console.log('resp:', resp);

    if (resp.ok) {
      const data = await resp.json();
      console.log('data:', data);

      MySwal.fire({
        icon: 'success',
        title: <span>Se registró correctamente.</span>,
        text: 'Se le redirigirá al login',
      }).then((result) => {
        if (result.isConfirmed) navigate('/auth/login');
      });
    } else {
      isDisabled(false);
      MySwal.fire({
        icon: 'info',
        title: <span>El correo que intenta ingresar ya existe</span>,
      });
    }
  };

  return (
    <main id="login">
      <div className="container">
        <img src={imgLogo} alt="Logo" width="125" />
        <h1>Buscar Mascota</h1>

        <form onSubmit={handleSubmit}>
          <h4>REGÍSTRATE</h4>
          <div className="form-group">
            <input
              className="form-control"
              type="text"
              name="nombreCompleto"
              placeholder="Ingresa tus nombres"
              maxLength="30"
              onChange={handleChange}
              pattern="[a-zA-Z\s]+"
              title="Ingreso solo letras"
              required
            />
          </div>

          <div className="form-group">
            <input
              className="form-control"
              type="text"
              name="apellidos"
              placeholder="Ingresa tus apellidos"
              onChange={handleChange}
              maxLength="30"
              pattern="[a-zA-Z\s]+"
              title="Ingreso solo letras"
              required
            />
          </div>

          <div className="form-group">
            <input
              className="form-control"
              type="email"
              name="correo"
              placeholder="Ingresa tu correo"
              onChange={handleChange}
              required
            />
          </div>

          <div className="form-group">
            <input
              className="form-control"
              type="text"
              name="celular"
              placeholder="Ingresa tu celular"
              minLength="9"
              maxLength="9"
              pattern="[0-9]+"
              title="Ingresa solo números"
              onChange={handleChange}
              required
            />
          </div>

          <div className="form-group form-pass">
            <input
              className="form-control"
              type={showPass ? 'text' : 'password'}
              name="clave"
              placeholder="Ingresa tu contraseña"
              minLength={6}
              onChange={handleChange}
              required
            />
            <div
              className={`eyePass ${showPass ? 'active' : ''}`}
              onClick={() => setShowPass(!showPass)}
            >
              <img src={icoEye} />
            </div>
          </div>

          <button className="btn" type="submit" disabled={disabled}>
            Registrar
          </button>
        </form>

        <br />

        <div className="redes-btn">
          <Link to="/auth/login">Iniciar sesión</Link>
        </div>
      </div>
    </main>
  );
};

export default Register;
