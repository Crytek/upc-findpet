import React, { useState } from 'react';
import { Link, useNavigate } from 'react-router-dom';

import { useAuthContext } from '../../context/AuthContext';
// import ReCAPTCHA from 'react-google-recaptcha';
import Recaptcha from 'react-google-invisible-recaptcha';

import Swal from 'sweetalert2';
import withReactContent from 'sweetalert2-react-content';
import 'sweetalert2/src/sweetalert2.scss';

// import FacebookLogin from 'react-facebook-login';
import { FacebookProvider, LoginButton } from 'react-facebook';

import imgLogo from '../../images/logo.png';
import icoFacebook from '../../images/facebook.svg';
import icoEye from '../../images/eye.svg';

const initialLogin = {
  correo: '',
  clave: '',
};

const recaptchaRef = React.createRef();

const Login = () => {
  const MySwal = withReactContent(Swal);

  const { authLogin } = useAuthContext();
  const [form, setForm] = useState(initialLogin);
  const [disabled, isDisabled] = useState(false);
  const [showPass, setShowPass] = useState(false);

  const navigate = useNavigate();

  const handleChange = (e) => {
    const { name, value } = e.target;

    setForm({
      ...form,
      [name]: value,
    });
  };

  const handleSubmit = async (e) => {
    e.preventDefault();
    isDisabled(true);

    const apiValidUser = `/api/User/GetValidateAccess?correo=${form.correo}&pass=${form.clave}`;

    const resp = await fetch(import.meta.env.VITE_API_URL + apiValidUser);

    console.log('resp:', resp);

    if (resp.ok) {
      const data = await resp.json();
      console.log('data:', data);

      if (data) {
        authLogin();
        navigate('/');
      } else {
        isDisabled(false);
        MySwal.fire({
          icon: 'info',
          title: <span>Credenciales inválidos</span>,
          text: 'Verifica tu correo y/o contraseña',
        });
      }
    } else {
      isDisabled(false);
      MySwal.fire({
        icon: 'error',
        title: <span>Ocurrió un error</span>,
      });
    }
  };

  const handleResponseF = (data) => {
    console.log('data:', data);
    authLogin();
    navigate('/');
  };

  const handleErrorF = (error) => {
    console.log('error:', error);
    MySwal.fire({
      icon: 'error',
      title: <span>Ocurrió un error</span>,
      text: 'Verifica que hayas aceptados los permisos',
    });
  };

  return (
    <>
      <Recaptcha
        ref={recaptchaRef}
        sitekey="6LeZaoIeAAAAAHNurbhiBCuvZD96EglDPDeIuIJG"
        onResolved={() => console.log('Human detected.')}
      />

      <main id="login">
        <div className="container">
          <img src={imgLogo} alt="Logo" width="125" />
          <h1>Buscar Mascota</h1>

          <form onSubmit={handleSubmit}>
            <h4>INICIAR SESIÓN</h4>
            <div className="form-group">
              <input
                className="form-control"
                type="email"
                name="correo"
                placeholder="Ingresa tu correo"
                onChange={handleChange}
                required
              />
            </div>

            <div className="form-group form-pass">
              <input
                className="form-control"
                type={showPass ? 'text' : 'password'}
                name="clave"
                placeholder="Ingresa tu contraseña"
                onChange={handleChange}
                minLength={6}
                required
                // pattern="(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{8,}"
                // title="Must contain at least one  number and one uppercase and lowercase letter, and at least 8 or more characters"
              />
              <div
                className={`eyePass ${showPass ? 'active' : ''}`}
                onClick={() => setShowPass(!showPass)}
              >
                <img src={icoEye} />
              </div>
            </div>

            <button className="btn" disabled={disabled}>
              Acceder
            </button>
          </form>

          <div className="redes-btn">
            <h4>o ingresa con</h4>

            {/* <FacebookLogin
              appId="475200957566400"
              autoLoad={true}
              xfbml={true}
              fields="name,email,picture"
              // onClick={componentClicked}
              callback={responseFacebook}
            /> */}

            <FacebookProvider appId="475200957566400">
              <LoginButton
                scope="email"
                onCompleted={handleResponseF}
                onError={handleErrorF}
                className="facebook-btn"
              >
                <img src={icoFacebook} alt="Facebook" width="30" />
                <b>FACEBOOK</b>
              </LoginButton>
            </FacebookProvider>

            <br />

            <Link to="/auth/register">Regístrate</Link>
          </div>
        </div>
      </main>
    </>
  );
};

export default Login;
