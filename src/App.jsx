import { BrowserRouter, Route, Routes } from 'react-router-dom';

import PrivateRoute from './routers/PrivateRoute';

import Login from './pages/auth/Login';
import Register from './pages/auth/Register';

import Publications from './pages/Publications';
import PublicationCreate from './pages/PublicationCreate';
import PublicationDetail from './pages/PublicationDetail';

import ProfilePage from './pages/Profile';

import './styles/app.scss';

function App() {
  return (
    <BrowserRouter>
      <Routes>
        <Route path="/auth/login" element={<Login />} />
        <Route path="/auth/register" element={<Register />} />

        <Route
          path="/"
          element={
            <PrivateRoute>
              <Publications />
            </PrivateRoute>
          }
        />
        <Route
          path="/publicacion/detalle"
          element={
            <PrivateRoute>
              <PublicationDetail />
            </PrivateRoute>
          }
        />
        <Route
          path="/publicacion/crear"
          element={
            <PrivateRoute>
              <PublicationCreate />
            </PrivateRoute>
          }
        />

        <Route
          path="/perfil"
          element={
            <PrivateRoute>
              <ProfilePage />
            </PrivateRoute>
          }
        />
      </Routes>
    </BrowserRouter>
  );
}

export default App;
