const getFetchData = async (url) => {
  const apiUrl = import.meta.env.VITE_API_URL + url;
  const response = await fetch(apiUrl);

  if (!response.ok) throw new Error('algo falló');

  const data = await response.json();

  return data;
};

export default getFetchData;
