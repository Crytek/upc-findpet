import React from 'react';

import Slider from 'react-slick';

import imgBanner1 from '../images/banner-home/1.png';
import imgBanner2 from '../images/banner-home/2.png';
import imgBanner3 from '../images/banner-home/3.png';

const Banner = () => {
  const settings = {
    dots: false,
    arrows: false,
    infinite: true,
    slidesToShow: 3,
    slidesToScroll: 1,
    autoplay: true,
    speed: 2000,
    autoplaySpeed: 2000,
    cssEase: 'linear',
  };

  return (
    <section id="banner">
      <Slider {...settings}>
        <div className="image">
          <img src={imgBanner1} alt="Banner1" />
        </div>
        <div className="image">
          <img src={imgBanner2} alt="Banner2" />
        </div>
        <div className="image">
          <img src={imgBanner3} alt="Banner3" />
        </div>
        <div className="image">
          <img src={imgBanner1} alt="Banner1" />
        </div>
        <div className="image">
          <img src={imgBanner2} alt="Banner2" />
        </div>
      </Slider>
    </section>
  );
};

export default Banner;
