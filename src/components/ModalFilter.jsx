import React, { useEffect, useState } from 'react';
import useFetchProducts from '../hook/useGetFetchData';

const initialFilter = {
  categoria_id: null,
  tipAnimal_id: null,
  raza_id: null,
  fec_inicio: null,
  fec_final: null,
};

const ModalFilter = ({ setModalFilter, handleSubmitFilter }) => {
  const dataCategories = useFetchProducts('/api/Categoria');
  const dataClasificacion = useFetchProducts('/api/Clasificacion');
  const dataRaza = useFetchProducts('/api/Raza');

  const [form, setForm] = useState(initialFilter);

  useEffect(() => {
    const today = new Date().toISOString().split('T')[0].split('-');
    const year = today[0];
    const month = today[1];
    const day = new Date().getDate();
    const currentDate = [year, month, day].join('-');
    document.getElementById('fec_inicio').setAttribute('max', currentDate);
    document.getElementById('fec_final').setAttribute('max', currentDate);
  }, []);

  const handleChangeNumber = (e) => {
    const { name, value } = e.target;

    setForm({
      ...form,
      [name]: Number(value),
    });
  };

  const handleChange = (e) => {
    const { name, value } = e.target;

    setForm({
      ...form,
      [name]: value,
    });
  };

  const handleSubmit = async (e) => {
    e.preventDefault();
    handleSubmitFilter(form);
  };

  return (
    <div className="modal">
      <div className="modal-content">
        <form onSubmit={handleSubmit}>
          <div className="modal-header">
            <h5>Búsqueda avanzada</h5>
            <button
              className="modal-button-close"
              type="button"
              onClick={() => setModalFilter(false)}
            >
              Cerrar(X)
            </button>
          </div>

          <div className="modal-body">
            <div className="form-group">
              <select
                id=""
                className="form-control"
                name="categoria_id"
                defaultValue=""
                onChange={handleChangeNumber}
                required
              >
                <option value="" disabled>
                  Selecciona una categoría
                </option>
                {dataCategories.data.map(({ descripcion, id }) => (
                  <option value={id} key={id}>
                    {descripcion}
                  </option>
                ))}
              </select>
            </div>

            <div className="form-group">
              <select
                id=""
                className="form-control"
                name="tipAnimal_id"
                defaultValue=""
                onChange={handleChangeNumber}
                required
              >
                <option value="" disabled>
                  Selecciona una tipo de animal
                </option>
                {dataClasificacion.data.map(({ descripcion, id }) => (
                  <option value={id} key={id}>
                    {descripcion}
                  </option>
                ))}
              </select>
            </div>

            <div className="form-group">
              <select
                id=""
                className="form-control"
                name="raza_id"
                defaultValue=""
                onChange={handleChangeNumber}
                required
              >
                <option value="" disabled>
                  Selecciona una raza
                </option>
                {dataRaza.data.map(({ descripcion, id }) => (
                  <option value={id} key={id}>
                    {descripcion}
                  </option>
                ))}
              </select>
            </div>

            <div className="form-group-row">
              <div className="form-group">
                <label htmlFor="">Desde</label>
                <input
                  id="fec_inicio"
                  className="form-control"
                  type="date"
                  name="fec_inicio"
                  onChange={handleChange}
                  required
                />
              </div>

              <div className="form-group">
                <label htmlFor="">Hasta</label>
                <input
                  id="fec_final"
                  className="form-control"
                  type="date"
                  name="fec_final"
                  onChange={handleChange}
                  required
                />
              </div>
            </div>
          </div>

          <div className="modal-footer">
            <button className="btn" type="submit">
              Filtrar
            </button>
          </div>
        </form>
      </div>
    </div>
  );
};

export default ModalFilter;
