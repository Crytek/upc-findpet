import React from 'react';

import imgPublicidad from '../images/publicidad.jpg';

const ModalBannerAds = ({ setModalBannerAds }) => {
  return (
    <div className="modal">
      <div className="modal-content">
        <div className="modal-header">
          <h5>Publicidad</h5>
          <button
            className="modal-button-close"
            type="button"
            onClick={() => setModalBannerAds(false)}
          >
            Cerrar(X)
          </button>
        </div>

        <div className="modal-body">
          <img src={imgPublicidad} alt="Publicidad" width="100%" />
        </div>
      </div>
    </div>
  );
};

export default ModalBannerAds;
