import React from 'react';
import { Link } from 'react-router-dom';

import imgLogo from '../images/logo.png';
import imgProfile from '../images/profile.png';

const Header = () => {
  return (
    <header id="header">
      <Link to="/">
        <img src={imgLogo} alt="FindPet" width={50} />
      </Link>

      <h1>BUSCA MASCOTAS</h1>

      <Link to="#!" className="btn-profile">
        <img src={imgProfile} alt="FindPet" width={45} />
      </Link>
    </header>
  );
};

export default Header;
