import { useEffect, useState } from 'react';
import getFetchData from '../services/getFetchData';

const useFetchProducts = (url) => {
  const initial = {
    data: [],
    loader: true,
  };

  const [state, setState] = useState(initial);

  useEffect(() => {
    getFetchData(url)
      .then((response) => {
        setState({
          data: response,
          loader: false,
        });
      })
      .catch((error) => console.error(error));
  }, [url]);

  return state;
};

export default useFetchProducts;
